SANFLAGS = -fsanitize=address
CFLAGS += -Wall -pedantic -g $(SANFLAGS)
LDFLAGS += $(SANFLAGS)
pastewithdifficulty:	main.o
	$(CC) -o $@ $^ $(LDFLAGS)
.phony:	clean
clean:
	$(RM) pastewithdifficulty *.o

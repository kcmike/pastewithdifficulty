#include <stdio.h>
#include <string.h>
#include <errno.h>
#include <stdlib.h>

int
main(int argc, char **argv)
{
	FILE *f;
	switch (argc) {
	default:
		printf("usage: %s <filename>\n", argv[0]);
		return 2;
	case 1:	f = stdin;	break;
	case 2:
		f = fopen(argv[1], "r");
		if (!f) {
			fprintf(stderr, "could not open '%s': %s\n", argv[1], strerror(errno));
			return 1;
		}
		break;
	}
	srand(1);	// don't need different output each time
	int c;
	while ((c = getc(f)) != EOF) {
		if (rand() % 16 == 0)					printf("​");	// zero-width space
		if (c == ' ') {
			switch (rand() % 8) {
			default:	putchar(c);		break;
			case 0:		printf(" ");	break;	// non-breaking space
			case 1:		printf(" ");	break;	// en-space
			case 2:		printf(" ");	break;	// figure space
			}
		} else if (c == ';' && rand() % 4 == 0)	printf(";");	// Greek question mark
		else if (c == 'i' && rand() % 4 == 0)	printf("ⅰ");	// Roman numeral 1
		else if (c == 'c' && rand() % 4 == 0)	printf("ⅽ");	// Roman numeral 100
		else									putchar(c);
	}
	return 0;
}

# pastewithdifficulty

This is a code garbler which makes your code uncompileable.
The intended use is when giving open-book code tracing questions on a quiz or exam, and you don't
want your students to be able to copy and paste directly into a compiler or interpreter to get
the answer.

Until someone comes along and makes a *de*garbler (could make for a fun assignment, actually),
students who want to "cheat" would have to type it out by hand, which would at least slow them
down a bit.

## Examples

```c
int
foo(int x)
{
	int c = 'a';
	return c + x;
}
```

would become

```c
int
foo(int x)
{
	int c = 'a';
	return c + x;
}
```

Look the same?
It should look *almost* identical visually, but the compiler will be unable to compile it.

## Transformations applied

The tool abuses Unicode and:

* inserts invisible whitespace
* changes spaces into things that look like spaces (such as non-breaking spaces or en-spaces)
* changes semicolons into Greek question marks
* changes quotes into things that look like quotes (like "right single quote")
* changes some letters into Roman numerals (e.g., "c" into "lowercase Roman numeral 100")

# Installing

## Requisites

You need make and a C compiler. On most Linux distributions, the build-essential package is more
than enough.

## Building

```sh
make
```

## Running

```sh
./pastewithdifficulty exam-question.c
```

or:

```sh
./pastewithdifficulty <exam-question.c
```
